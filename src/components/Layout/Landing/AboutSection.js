import React from 'react'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
    root: {
      marginTop: 180,
      marginBottom: 200,
    },
    typography: {
        textAlign:"center", 
        fontWeight:"bolder"
    }

  }));
  

const AboutSection = () => {
    const classes = useStyles();
    return (
      
        <Grid container className={classes.root} >
            <Grid item sm={12} md={12} lg={3}>
                <Typography variant="h3" className={classes.typography}>
                    About us
                </Typography>
                
            </Grid>
            <Grid item sm={12} md={12} lg={6} className={classes.typography}>
                <Typography variant="h6" >
                    DIGITAL TRANSFORMATION OF YOUR BUSINESS
                </Typography>
                <Typography variant="title">
                    We aim to deliver top-tier solutions tailored to the needs of organization and companies of all sizes. Our mission is to help you drive high-impact digital transformation by taking full advantage of 
                    exponential technology change and getting to the core of multiple opportunities quickly and efficiently. 
                </Typography>
            </Grid>
            <Grid item sm={12} md={12} lg={3} className={classes.typography}>
                <Typography variant="body2">
                    Sheldon Moreno
                </Typography>
                <Typography variant="body2">
                    HEAD OF OPERATIONS
                </Typography>
            </Grid>
        </Grid>
        
    )
}
export default AboutSection;
