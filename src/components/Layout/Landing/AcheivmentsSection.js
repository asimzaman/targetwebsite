import React from 'react'
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';



const useStyles = makeStyles(theme => ({
    root: {
      marginTop: 40,
      marginRight: theme.spacing(2),
      
    },
    paper: {
        padding: theme.spacing(2),
        marginTop : theme.spacing(5),
        marginBottom : theme.spacing(5),
        color: theme.palette.text.secondary,
        background: "#1b4e8f",
        borderRadius: 0
      },
    typographyContent: {
        textAlign:'center', 
        fontWeight: "bolder" , 
        fontSize:30, 
        color:"white"
    },
    dividerFirst : {
        width:"10%",
        backgroundColor :"white",
        height:5, 
        marginLeft:"45%",
        marginTop:20
    }
}));

export default function AcheivmentsSection() {
    const classes = useStyles();
    return (
        <React.Fragment>
        <Paper className={classes.paper}>
            <Grid container direction="row" justify="center"> 
                <img src="https://irp-cdn.multiscreensite.com/2c98796a/dms3rep/multi/background/Meera+Logo.png" />
            </Grid>
            <Grid container direction="row" justify="center">
                <Typography variant="h6" className={classes.typographyContent}>
                    A Digital Workplace for Realizing Transformational,<br/>
                    Enterprise Workflows
                </Typography>
            </Grid>
            <Grid container direction="row" justify="center" alignItems="center"> 
                <Grid item sm={12} lg={8} style={{textAlign:'center',  fontWeight: "bolder" , color:"white" , marginTop:20}}>
                    <Typography variant="body">
                        Built on top of cloud application architecture and deployed on a
                        private, public, or hybrid cloud, the ecosystem is an interactive,
                        multi-user environment that supports seamless workflows
                        across business process silos and supports regulated, repeatable, and documented business functions with related
                        programmable services
                    </Typography>
                    <Divider className={classes.dividerFirst}/>
                    <Typography variant="h6" style={{marginTop:20}}>
                        MEERA Ecosystem
                    </Typography>
                </Grid>
            </Grid>
            <Grid container direction="row" justify="center" alignItems="center" style={{marginTop:20}}> 
                <Grid item sm={12} lg={10}>
                    <img src="https://irp-cdn.multiscreensite.com/2c98796a/dms3rep/multi/background/ecosystem4.jpg" width="100%"/>
                </Grid>
                <Grid item sm={12} lg={10} className={classes.typographyContent}>
                    <Typography variant="h6">
                        MEERA for E&P - INDUSTRY FIRST - E&P APP STORE <br/> 
                        USING OPEN API AND SDK FOR 3RD PARTY APPS
                    </Typography>
                </Grid>
                <Grid item sm={12} lg={10} style={{marginTop:20}} >
                    <img src="https://irp-cdn.multiscreensite.com/2c98796a/dms3rep/multi/background/e%26Pappstore.png" width="100%" height="900"/>
                </Grid>
            </Grid>
        </Paper>
        <Grid container direction="row" justify="center" alignItems="center"> 
            <Grid item sm={12} lg={8}>
                <Divider style={{height:5}}/>
            </Grid>
        </Grid>
        </React.Fragment>
    )
}
