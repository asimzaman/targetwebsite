import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import '../../../../src/carousel.css';


const AutoplaySlider = withAutoplay(AwesomeSlider);

const useStyles = makeStyles(theme => ({
    root : {
        position:"relative",
        zIndex:"-10"
    },
   
 }));

const Carousel = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <AutoplaySlider
                play={true}
                bullets={false}
                cancelOnInteraction={false} // should stop playing on user interaction
                interval={1000}
            >
                <div data-src="https://www.fairobserver.com/wp-content/uploads/2019/08/Tech-news.jpg" />         
                <div data-src="https://thumbor.forbes.com/thumbor/960x0/https%3A%2F%2Fspecials-images.forbesimg.com%2Fdam%2Fimageserve%2F1169465095%2F960x0.jpg" />
                <div data-src="https://img.freepik.com/free-vector/technology-geometric-background_29971-339.jpg?size=626&ext=jpg" />
            </AutoplaySlider>
        </div>
    )
}
export default Carousel;
