import React from 'react'
import Carousel from './Carousel';
import AboutSection from './AboutSection';
import ProjectSection from './ProjectSection';
import ClientSection from './ClientSection';
import AcheivmentsSection from './AcheivmentsSection';
import CarouselContent from './CarouselContent';

const Landing = () => {
    return (
        <React.Fragment>
            <CarouselContent />
            <Carousel />
            <AboutSection />
            <ProjectSection />
            <AcheivmentsSection />
            <ClientSection />
        </React.Fragment>
       
    )
}

export default Landing; 
