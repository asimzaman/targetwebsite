import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
   root : {
        marginTop:180,
        marginLeft:200, 
        position:"absolute",
        zIndex:"10" , 
        color:"white"
   },
   divider : {
    backgroundColor :"white",
    fontWeight:"bolder",
    marginTop:10, 
    marginBottom:10
   },
   typoHeading : {
       fontWeight: "bolder"
   }
}));

const CarouselContent = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Typography variant="h3" className={classes.typoHeading}>
                TARGET
            </Typography>
            <Divider className={classes.divider}/>
            <Typography variant="h5">
                Makes Chaos to Cool
            </Typography>

        </div>
    )
}
export default CarouselContent;
