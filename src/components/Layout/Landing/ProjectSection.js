import React from 'react'
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Slide from 'react-reveal/Slide';
import Divider from '@material-ui/core/Divider';
import GroupIcon from '@material-ui/icons/Group';

const useStyles = makeStyles(theme => ({
    root: {
      marginTop: 120,
      backgroundColor:"#ecedef"
      
    },
    paper: {
        padding: theme.spacing(2),
        margin: theme.spacing(2),
        textAlign: 'center',
        height: 500,
        color: theme.palette.text.secondary,
        background: "#1b4e8f",
        
      },
      typographyHead: {
        marginTop:40, 
        fontWeight: "bolder" , 
        fontSize:30, 
        color:"#0eb0ee"
      },

      typographyContent : {
        marginTop:25, 
        fontWeight: "bolder" ,  
        color:"white"
      },
      image:{
          borderRadius: "50%"
      }
}));

const ProjectSection = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
        <Grid container direction="row" justify="center" >
            <Grid item xs={12} lg={3} style={{marginTop:50}}>
                <Slide bottom>
                    <Typography variant="6" style={{ marginLeft:50, fontWeight: "bolder" , fontSize:30}}>
                        Our projects
                    </Typography>
                    
                    <Typography variant="6" style={{ marginLeft:50, fontWeight: "bolder" , fontSize:30, color:"#0eb0ee"}}>
                        & missions 
                    </Typography>
                        <img src="http://webdesign-finder.com/html/oil/images/map-scheme.png"  style={{marginTop:100, marginLeft:20}}/>
                    <Typography style={{ marginLeft:50, marginTop:50, fontWeight: "bolder" , fontSize:10, color:"#0eb0ee"}}>3560
                        COMPLETED PROJECTS
                    </Typography>
                </Slide>    
            </Grid>
            <Grid item xs={12} lg={3}>
                <Slide bottom>
                    <Paper className={classes.paper}>
                        <img src="http://webdesign-finder.com/html/oil/images/icon1.png" className={classes.image} />
                        <Typography className={classes.typographyHead}>
                            Out Team
                        </Typography>
                        <Typography className={classes.typographyContent}>
                            TARGET’s teams around the world are developing technologies and building solutions to help organizations manage their digital transformation. 
                            The company has a robust software portfolio and a long history of technology leadership.
                        </Typography>
                    </Paper>
                </Slide>
            </Grid>
            <Grid item xs={12} lg={3}>
                <Slide bottom>
                <Paper className={classes.paper}>
                        <img src="http://webdesign-finder.com/html/oil/images/icon2.png" className={classes.image} />
                        
                        <Typography className={classes.typographyHead}>
                            Our Story
                        </Typography>
                        <Typography className={classes.typographyContent}>
                        We started developing solutions over 15 years ago. Today our solutions help a broad range of clients from policy makers, regulators, service providers and operators, to financial institutions and investors across a variety of sectors. 
                        Today our company is over 750 people strong and growing. 
                        </Typography>
                       

                    </Paper>
                </Slide>
            </Grid>
            <Grid item xs={12} lg={3}>
                <Slide bottom>
                <Paper className={classes.paper}>
                        <img src="http://webdesign-finder.com/html/oil/images/icon2.png" className={classes.image} />
                        
                        <Typography className={classes.typographyHead}>
                        Our Vision</Typography>
                        <Typography className={classes.typographyContent}>
                        In the future every stakeholder will have a place to work where all the data they need will be accessible, regardless of the source, business domain or technical class of that data. That workspace will be universal, centrally accessible, online, and collaborative. 
                        Analysis will be performed directly on shared data, rather than on disconnected fragments. 
                        </Typography>
                       

                    </Paper>
                </Slide>
            </Grid>
           
        </Grid>
        </div>
    )
}
export default ProjectSection;
