import React from 'react'
import { Parallax, Background } from 'react-parallax';
import { Typography, Grid } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';

import nature from '../../../../src/nature.jpg';


export default function ClientSection() {
    return (
        <div >
        <Grid container direction="row" justify="center" style={{marginTop:20}}>
            <Grid item lg={12}>
                <Parallax strength={500}>
                {/* blur={{ min: -15, max: 15 }} */}
                <div style={{ textAlign: 'center', height:500,backgroundColor:'#30355dab' , color:'white',position: 'relative'}}>
                    <Typography variant="h6">
                        MEERA for E&P - INDUSTRY FIRST - E&P APP STORE <br/> 
                        USING OPEN API AND SDK FOR 3RD PARTY APPS
                    </Typography>
                    <img src="https://irp-cdn.multiscreensite.com/2c98796a/dms3rep/multi/tablet/logos2.png" height="80%"/>
                </div>
                <Background className="custom-bg">
                    <img src="https://www.e-spincorp.com/wp-content/uploads/2018/06/digital-transformation-espincorp.jpg" alt="fill murray"  />
                </Background>
                </Parallax>
            </Grid>
        </Grid>
        <Grid container direction="row" justify="center" alignItems="center" style={{marginTop:20, marginBottom:20}}> 
            <Grid item sm={12} lg={8}>
                <Divider style={{height:5}}/>
            </Grid>
        </Grid>
        </div>
    )
}
