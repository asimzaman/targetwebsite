import React from 'react';
import { Typography, Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import YouTubeIcon from '@material-ui/icons/YouTube';
import ContactForm from './../../Pages/ContactForm';


const useStyles = makeStyles(theme => ({
    footer: {
      marginTop: 'auto',
      height:'auto',
      backgroundColor: "#0eb0ee",
      textAlign: "center",
     
    },
    typographyHeaders: {
        textAlign:"center",
        color:"white", 
        marginTop:20
    },
    icons: {
        fontSize: 40, 
        color:"white"
    }
  }));
const Footer = () => {
    const classes = useStyles();
    return (
      <React.Fragment> 
            <Grid container className={classes.footer} >
                <Grid  item xs={12} sm={12} md={4} lg={4} xl={4}>
                    <Typography variant="h5" className={classes.typographyHeaders}>
                        Target Technologies
                    </Typography>
                    <Typography variant="h5" style={{textAlign:"left",color:"white", marginTop:20, marginLeft:"30%"}}>
                            Everyday is a new day for us and we work really hard to satisfy our customer everywhere.
                    </Typography>
                    <Typography style={{textAlign:'left',marginLeft:"30%", marginTop:20}}>
                        <FacebookIcon  className={classes.icons} />
                        <TwitterIcon  className={classes.icons}/>
                        <YouTubeIcon  className={classes.icons}/>
                    </Typography>   
                </Grid>
  
                
            <Grid  item xs={12} sm={12} md={8} lg={8} xl={8} style={{textAlign:'left'}}>
                  <Typography variant="h5" className={classes.typographyHeaders}>
                          LET's WORKTOGETHER. CONTACT US.
                  </Typography>
                  <Grid container >
                  <Grid item xs={12} sm={12} lg={4} xl={4}>
                      <Typography variant="h6" className={classes.typographyHeaders}>
                            EMAIL US
                      </Typography>
                      <Typography variant="h6" className={classes.typographyHeaders}>
                            INFO@TARGET-ENERGYSOLUTIONS.COM
                      </Typography>
                  </Grid>
                  <Grid item xs={12} sm={12} lg={8} xl={8}>
                      <Typography variant="6" className={classes.typographyHeaders}>
                          Want More Info Or A Demo?
                      </Typography>
                      <ContactForm />
                  </Grid>
                  </Grid>
            </Grid>
              
          </Grid>
         
            
           
         
      </React.Fragment>
    )
}
export default Footer;
