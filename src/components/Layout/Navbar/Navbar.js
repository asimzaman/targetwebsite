import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import logo from '../../../../src/logo.png';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    
  },
  appBar : {
    position: "fixed",
    padding:20,
    backgroundColor: "#0eb0ee",
    height:100
  },

  MenuIcon : {
      shadows: 'none'
  },
  
  
  title: {
    marginRight: theme.spacing(3)
  },

  link:{
    textDecoration: 'none', 
    color:"white"
  },
  iconButton: {
    marginRight: 15
  },
  logo :{
    height: 40,
  }

}));

const Navbar = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={ classes.appBar}>
        <Toolbar >
          <Link to='/' className={classes.link}>
            <IconButton className={classes.iconButton}>
              <img  src={logo}  className={classes.logo}></img>
            </IconButton>
          </Link>
          <Link to='/' className={classes.link}>
            <Typography variant="h6" className={classes.title}>
              Home
            </Typography>
          </Link>
          <Link to='/meera-ecosystem' className={classes.link}>
            <Typography variant="h6" className={classes.title}>
              MEERA Ecosystem 
            </Typography>
          </Link>
          <Link to='/ep-solution' className={classes.link}>
          <Typography variant="h6" className={classes.title}>
             E&P Solutions 
          </Typography>
          </Link>
          <Link to='/professional-services' className={classes.link}>
            <Typography variant="h6" className={classes.title}>
              Professional Services 
            </Typography>
          </Link>
          <Link to='/news' className={classes.link}>
          <Typography variant="h6" className={classes.title}>
             News 
          </Typography>
          </Link>
          <Link to='/contact' className={classes.link}>
          <Typography variant="h6" className={classes.title}>
             Contact 
          </Typography>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
}
export default Navbar;