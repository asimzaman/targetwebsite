import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MeeraEcoPic from './../../assets/MeeraEco.PNG'
import { Paper, Typography, Divider } from '@material-ui/core';


const useStyles = makeStyles(theme => ({
    typoHeading:{
        textAlign:"center",
        color:"#0eb0ee", 
        marginTop:20
    },
    typoContent: {
        textAlign:"left", 
        marginTop:20, 
        marginBottom:20
    },
    divider:{
        backgroundColor:"#0eb0ee" , 
        height:3, 
        marginTop:20, 
        marginBottom:20
    }
}));

const MeeraEco = () => {
    const classes = useStyles();
    return (
        <div>
            <Grid container >
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} style={{marginTop:100}}>
                    <img src={MeeraEcoPic} width="100%"/>
                </Grid>
            </Grid>
            
              <Paper elevation="0" style={{borderRadius:0}}>
                <Grid container >
                        <Grid item sm={12} lg={12} style={{marginTop:60}}>
                            <Typography variant="h4" className={classes.typoHeading}>MEERA ECOSYSTEM</Typography>
                        </Grid>
                </Grid>
                <Grid container direction="row" justify="center">
                        <Grid item sm={12} lg={6}>
                            <Typography variant="h6" style={{textAlign:"center", marginTop:20, marginBottom:50}}>
                                A digital workplace for realizing transformational, enterprise workflows. 
                                Built on top of cloud application architecture and deployed on a private, public, or hybrid cloud, the ecosystem is an interactive, multi-user environment that supports seamless workflows across business silos.
                            </Typography>
                        </Grid>
                </Grid>
                <Grid container direction="row" justify="space-around">
                    
                        <Grid item sm={12} lg={5} style={{marginTop:60,marginBottom:30}}>
                            <img src="https://scitechdaily.com/images/Signal-Processing-Concept-777x518.jpg" width="95%"/>           
                        </Grid>
                    
                        <Grid item sm={12} lg={5} style={{marginTop:60,marginBottom:30}}>
                            <Typography variant="h4" style={{textAlign:"center",color:"#0eb0ee"}}>
                                Digital Transformation <br/>
                                Making Chaos Cool!
                            </Typography>
                            <Typography variant="body" style={{ marginTop:40, textAlign:"left"}}>
                                Business leaders believe that a digital workplace effectively planned and implemented will 
                                enable employees to achieve business outcomes faster and 
                                empower employees to reduce cost and increase efficiency.
                                To be successful, your workplace strategy must consider social 
                                foundation, data management and business applications 
                                in the transformation. Your social foundation strategy should 
                                include 3 pillars of interoperability.
                                <ol style={{textAlign:"left"}}> 
                                    <li>Personal Productivity</li>
                                    <li>Team Productivity</li>
                                    <li>Organizational Productivity</li>
                                </ol>
                            </Typography>
                        </Grid>
                   
                </Grid>
            </Paper>
            <Grid container direction="row" justify="center">
                <Grid item xs={12} sm={12} md={6} lg={8} xl={8} >
                    <Divider className={classes.divider}/>
                </Grid>
            </Grid>
            <Grid container direction="row" justify="space-around">
                <Grid item xs={12} sm={12} md={3} lg={3} xl={3} >
                    <Typography variant="h6" className={classes.typoHeading}>MEERA ECOSYSTEM</Typography>
                
                    <Typography variant="h6" className={classes.typoContent}>
                                A digital workplace for realizing transformational, enterprise workflows. 
                                Built on top of cloud application architecture and deployed on a private, public, or hybrid cloud, the ecosystem is an interactive, multi-user environment that supports seamless workflows across business silos.
                    </Typography>
                    <img src="https://irp-cdn.multiscreensite.com/2c98796a/dms3rep/multi/mobile/shutterstock_563081326-6eb79983.jpg" width="90%"/>
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3} xl={3} >
                    <Typography variant="h6" className={classes.typoHeading}>MEERA ECOSYSTEM</Typography>
                    
                    <Typography variant="h6" className={classes.typoContent}>
                        Includes a comprehensive API that allows innovators to integrate any application into their transformed business processes.
                        Follows API centric approach, components can be swapped and easily integrated.
                        Common services included, heavy focus on oil and gas business related services.
                        Flexible development model: configure interface style, build your own workflows, develop your own modules.
                        Component library that helps you build tailor made interfaces.
                        Business Process Management services included.
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3} xl={3} >
                    <Typography variant="h6" className={classes.typoHeading}>MEERA ECOSYSTEM</Typography>
                    
                    <Typography variant="h6" className={classes.typoContent}>
                        Bring together your isolated data and workflows into an industry standard relational database with ease.
                        Federated login & fine-grained access control.
                        Cloud native file storage with S3 API that integrates with access control system.
                        Internal & external notification services.
                        Virtual data model allows you to configure views of tables with updating capability.
                        Open Standards Compliant (OSDU, PPDM, etc).
                    </Typography>
                </Grid>
            </Grid>
            <Grid container direction="row" justify="center">
                <Grid item xs={12} sm={12} md={6} lg={8} xl={8} >
                    <Divider className={classes.divider}/>
                </Grid>
            </Grid>
            
        </div>
            
            
        
    )
}
export default MeeraEco;
