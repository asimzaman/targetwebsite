import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import newPic from './../../assets/newPic.PNG';
import { Typography, Divider } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    typoHeading:{
        textAlign:"center",
        color:"#0eb0ee", 
        marginTop:20
    },
    typoContent: {
        textAlign:"left", 
        marginTop:20, 
        marginBottom:20
    },
    divider:{
        backgroundColor:"#0eb0ee" ,
        height:3,
        marginTop:20, 
        marginBottom:20
    }
}));


const News = () => {
    const classes = useStyles();
    return (
        <div>
            <Grid container >
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} style={{marginTop:100}}>
                    <img src={newPic} width="100%"/>
                </Grid>
            </Grid>
            
              <Grid container >
                    <Grid item sm={12} lg={12} style={{marginTop:60}}>
                        <Typography variant="h4" className={classes.typoHeading}>New Releases</Typography>
                    </Grid>
                </Grid>
                    
                    
                       
                    
                        
            <Grid container direction="row" justify="center">
                <Grid item xs={12} sm={12} md={6} lg={8} xl={8} >
                    <Divider className={classes.divider}/>
                </Grid>
            </Grid>
            <Grid container direction="row" justify="center">
                <Grid item xs={12} sm={12} md={8} lg={8} xl={8} >
                    <Typography variant="h6" style={{textAlign:"left",color:"#0eb0ee", marginTop:20}}>
                        TARGET announces today the official release of its MEERA Simulation Package.
                    </Typography>
                    <Typography variant="h6" className={classes.typoContent}>
                        Press Release
                    </Typography><br/>
                
                    <Typography variant="body" className={classes.typoContent}>
                        [MUSCAT, OMAN] – [6, January 2020] – TARGET announces today the official release of its MEERA simulation package. A revolutionary break through that combines AI and numerical simulation models in one framework making it the first AI-Physics augmented reservoir simulator.

                        MEERA Simulator is a conventional 3D, 3-phase numerical reservoir simulator which guarantees mass conservation for all compositions within the reservoir and wells using flux conserved form of finite volume discretization for governing Navier-Stokes equations. The actual simulation is performed on a multi-scale grid with arbitrary up-scaled grid block properties.

                        The AI/ML Engine Is a multi-layer deep learning framework having fully connected networks in conjunction with various drop-out layers and coupled with enhanced LSTM based recurrent neural networks.

                        ‘We are extremely proud to deliver to market this extraordinary intelligent reservoir simulator’ said Ali Al Mujaini, TARGET CEO. With this our clients can now produce;

                        - Better alignment between development and operation plans and provide stable and auditable cash flow predictions to their shareholders.

                        - Up-to-date and reliable remaining hydrocarbon maps to optimize infill drilling locations between FDP cycles.

                        - Better optimization of development strategies by running numerous scenarios in minutes.

                        TARGET is an international technology and services company focused on delivering digital transformation solutions to data-driven industries. We work with a broad range of clients from policy makers, regulators, service providers and operators, to financial institutions and investors across a variety of industrial sectors. Whilst TARGET’s core business has its origins in the upstream oil and gas sector, our technology solutions are scalable, expandable and portable across other extractive industries.

                        For more information on Target Energy: http://www.target-energysolutions.com/ 
                        For MEERA Simulation: meerasimulation.com
                    </Typography>
                    
                </Grid>
                
               
            </Grid>
            <Grid container direction="row" justify="center">
                <Grid item xs={12} sm={12} md={6} lg={8} xl={8} >
                    <Divider className={classes.divider}/>
                </Grid>
            </Grid>
           
            
        </div>
            
            
        
    )
}
export default News;