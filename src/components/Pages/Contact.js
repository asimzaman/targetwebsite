import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import contactUs from './../../assets/contactUs.PNG';
import { Typography } from '@material-ui/core';
import ContactFormPage from './ContactFormPage';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop:100
    },
    contactContainer:{
        marginTop:40, 
        marginBottom:40
    },
    contactHeading: {
        textAlign:"left",
        color:"#0eb0ee", 
        marginTop:20
    },
    contactAddress:{
        textAlign:"left", 
        marginTop:10,
        marginBottom:20
    }


  }));

const Contact = () => {
    const classes = useStyles();
    return (
        <div >
            <Grid container >
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} className={classes.root}>
                    <img src={contactUs} width="100%"/>
                </Grid>
            </Grid>       
                        
            <Grid container direction="row" justify="space-around" className={classes.contactContainer}>
                <Grid item xs={12} sm={12} md={4} lg={4} xl={4} >
                    <Typography variant="h6" className={classes.contactHeading}>
                        CONTACT INFO
                    </Typography><br/>
                    <Typography variant="h6" className={classes.contactAddress}>
                        We’d love to hear about your digital transformation needs. To help us get an idea of what you need, 
                        please fill in this contact form and we’ll get right back to you.
                    </Typography>
                    <Typography variant="h6" className={classes.contactHeading}>
                        Global Locations
                    </Typography><br/>
                    <Typography variant="h6" className={classes.contactHeading}>
                        Oman Office
                    </Typography><br/>
                    <Typography variant="body" className={classes.contactAddress}>
                        TARGET <br/>
                        Beach One Building, 6th floor, Office 609<br/>
                        P.O Box: 418, P.C: 118<br/>
                        Sarooj, Muscat,<br/>
                        Sultanate of Oman<br/>

                        Email: info@target-energysolutions.com<br/>
                        Phone: +968 24649012<br/>
                    </Typography>

                    <Typography variant="h6" className={classes.contactHeading}>
                        United Kingdom Office
                    </Typography><br/>
                    <Typography variant="body" className={classes.contactAddress}>
                        TARGET <br/>
                        Dukes Court<br/>
                        Duke Street Woking Surrey<br/>
                        Sarooj, Muscat,<br/>
                        GU21 5BH<br/>
                        United Kingdom<br/>


                        Email: info@target-energysolutions.com<br/>
                        Phone: +44 1483 762816<br/>
                    </Typography>
                    <Typography variant="h6" className={classes.contactHeading}>
                        Oman Office
                    </Typography><br/>
                    <Typography variant="body" className={classes.contactAddress}>
                        TARGET <br/>
                        Beach One Building, 6th floor, Office 609<br/>
                        P.O Box: 418, P.C: 118<br/>
                        Sarooj, Muscat,<br/>
                        Sultanate of Oman<br/>

                        Email: info@target-energysolutions.com<br/>
                        Phone: +968 24649012<br/>
                    </Typography>

                    <Typography variant="h6" className={classes.contactHeading}>
                        United Kingdom Office
                    </Typography><br/>
                    <Typography variant="body" className={classes.contactAddress}>
                        TARGET <br/>
                        Dukes Court<br/>
                        Duke Street Woking Surrey<br/>
                        Sarooj, Muscat,<br/>
                        GU21 5BH<br/>
                        United Kingdom<br/>


                        Email: info@target-energysolutions.com<br/>
                        Phone: +44 1483 762816<br/>
                    </Typography>
                    <Typography variant="h6" className={classes.contactHeading}>
                        Oman Office
                    </Typography><br/>
                    <Typography variant="body" className={classes.contactAddress}>
                        TARGET <br/>
                        Beach One Building, 6th floor, Office 609<br/>
                        P.O Box: 418, P.C: 118<br/>
                        Sarooj, Muscat,<br/>
                        Sultanate of Oman<br/>

                        Email: info@target-energysolutions.com<br/>
                        Phone: +968 24649012<br/>
                    </Typography>

                    <Typography variant="h6" className={classes.contactHeading}>
                        United Kingdom Office
                    </Typography><br/>
                    <Typography variant="body" className={classes.contactAddress}>
                        TARGET <br/>
                        Dukes Court<br/>
                        Duke Street Woking Surrey<br/>
                        Sarooj, Muscat,<br/>
                        GU21 5BH<br/>
                        United Kingdom<br/>


                        Email: info@target-energysolutions.com<br/>
                        Phone: +44 1483 762816<br/>
                    </Typography>
                    
                </Grid>
                <Grid item xs={12} sm={12} md={5} lg={5} xl={5} >
                    <Typography variant="h6" className={classes.contactHeading}>
                        CONTACT FORM
                    </Typography>
                    <ContactFormPage />
                    
                </Grid>
                
               
            </Grid>
           
            
        </div>
            
            
        
    )
}
export default Contact;
