import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: "50%",
    },
  },
}));

const ContactForm = () => {
  const classes = useStyles();

  return (
    <form className={classes.root} noValidate autoComplete="off" >
      <TextField id="outlined-basic" label="Name " variant="outlined" />
      <TextField id="outlined-basic" label="Email " variant="outlined" />
      <TextField id="outlined-basic" label="Phone " variant="outlined" />
      <TextField
          id="outlined-multiline-static"
          label="Message "
          multiline
          rows="3"
          variant="outlined"
        />
      <Button variant="outlined" color="primary" style={{width:"50%"}}>
            Submit
      </Button>
    </form>
  );
}
export default ContactForm;
