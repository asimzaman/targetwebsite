import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ps from './../../assets/ps.PNG'
import { Paper, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    typoHeading:{
        textAlign:"center",
        color:"#0eb0ee", 
        marginTop:20
    },
    typoContent: {
        textAlign:"left", 
        marginTop:20

    },
}));


const ProfessionalServices = () => {
    const classes = useStyles();
    return (
        <div>
             <Grid container >
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} style={{marginTop:100}}>
                    <img src={ps} width="100%"/>
                </Grid>
            </Grid>
            
             
              <Grid container  style={{marginTop:30,marginBottom:30}}>
                    <Grid item sm={12} lg={12}>
                        <Typography variant="h4" className={classes.typoHeading}>
                            PROFESSIONAL SERVICES
                        </Typography>
                    </Grid>
                    <Grid container direction="row" justify="center">
                        <Grid item sm={12} lg={6}>
                            <Typography variant="h6" style={{textAlign:"center", marginTop:20}}>
                            The MEERA Professional Services team share a life-long passion for bringing innovation and technology to the business as well as developing people. As strategic E&P leaders and former executives of supermajor IOC’s, we know how to blend process, people and technology to meet your business goals.
                            We have successfully led the digital transformation and change management journey of a variety of customers and scales. 
                            </Typography>
                        </Grid>
                    </Grid>  
                   
                </Grid>
            
            <Paper elevation="0" style={{background:"#f4f4f4", marginTop:30,marginBottom:30}}>
            <Grid container >
                    <Grid item sm={12} lg={12}>
                        <Typography variant="h5" className={classes.typoHeading}>
                          DIGITAL TRANSFORMATION STARTS WITH OBJECTIVES AND WORKFLOWS
                        </Typography>
                    </Grid>
                    <Grid container direction="row" justify="space-around"  style={{marginTop:30,marginBottom:30}}>
                        <Grid item sm={12} lg={6}>
                            <img src="https://irp-cdn.multiscreensite.com/2c98796a/dms3rep/multi/desktop/DTObjectives.png" width="90%" />
                        </Grid>
                        <Grid item sm={12} lg={3}>
                            <Typography variant="h5" className={classes.typoHeading}>
                             OUR PHILOSOPHY
                            </Typography>
                            <Typography variant="h6" className={classes.typoContent}>
                                The Digital Transformation Service is structured around customer business outcomes. Projects start with a specific client goal, developed into a tailored solution integrating governance aspects,
                                business process, people, data and systems elements to assure aspired results are met. Business solutions are delivered to your user community as a website inside your network or in the cloud. These solutions securely publish and manage collections of data, workflows and tools according to your specifications to digitally transform your business.
                            </Typography>
                        </Grid>
                        <Grid item sm={12} lg={3}>
                            <Typography variant="h5" className={classes.typoHeading}>
                                OUR PROFESSIONAL SERVICES
                            </Typography>
                            <Typography variant="h6" className={classes.typoContent}>
                            The MEERA Professional Services team share a life-long passion for bringing innovation and technology to the business as well as developing people. As strategic E&P leaders and former executives of supermajor IOC’s, we know how to blend process, people and technology to meet your business goals.
                            We have successfully led the digital transformation and change management journey of a variety of customers and scales. 
                            </Typography>
                        </Grid>
                    </Grid>  
                   
                </Grid>
            </Paper>
            
            
        </div>
            
            
        
    )
}
export default ProfessionalServices;
