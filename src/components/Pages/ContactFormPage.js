import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: "90%",
    },
  },
  messageButton: {
    width:"50%", 
    marginLeft:"41%"
  }
}));

const ContactFormPage = () => {
  const classes = useStyles();

  return (
    <form className={classes.root} noValidate autoComplete="off" >
      <TextField id="outlined-basic" label="Name " variant="outlined" />
      <TextField id="outlined-basic" label="Company " variant="outlined" />
      <TextField id="outlined-basic" label="Title " variant="outlined" />
      <TextField id="outlined-basic" label="Email " variant="outlined" />
      <TextField id="outlined-basic" label="Phone " variant="outlined" />
      <TextField id="outlined-basic" label="How Can We Help " variant="outlined" />
      <TextField
          id="outlined-multiline-static"
          label="Message "
          multiline
          rows="5"
          variant="outlined"
        />
        <Button variant="outlined" color="primary" className={classes.messageButton}>
            Send Message
        </Button>
    </form>
  );
}
export default ContactFormPage;
