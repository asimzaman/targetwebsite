import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import epsol from './../../assets/epsol.PNG'
import { Paper, Typography,Divider } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
   typographyHeading:{
    textAlign:"center",
    color:"#0eb0ee", 
    marginTop:20
   },
   typographyContent:{
    textAlign:"left", 
    marginTop:20,
    marginBottom:20
   }
  }));


const  EPSolution = () => {
    const classes = useStyles();
    return (
        <div>
             <Grid container >
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} style={{marginTop:100}}>
                    <img src={epsol} width="100%"/>
                </Grid>
            </Grid>
            
              <Paper elevation="0" >
                    <Grid container direction="row" justify="space-around" style={{marginTop:50}}>
                            <Grid item sm={12} lg={12} >
                                <Typography variant="h4" className={classes.typographyHeading}>
                                    DIGITAL TRANSFORMATION FOR THE <br/>
                                    OIL AND GAS INDUSTRY
                                </Typography>
                            </Grid>
                    </Grid>
            
                    <Grid container direction="row" justify="center">
                        <Grid item sm={12} lg={6}>
                            <Typography variant="h6" style={{textAlign:"center", marginTop:20}}>
                            Our vision is to offer solutions that digitally transform all aspects of the oil and gas industry across the entire value chain. We believe that true digital transformation, unlocking and integrating every bit of data in wells, reservoirs and fields, can enable countries and 
                            E&P companies to maximize their returns by increasing their exploration success and maximizing hydrocarbon recovery.
                            </Typography>
                        </Grid>
                    </Grid>
                    
                <Grid container direction="row" justify="space-around" style={{marginTop:50}}>
                    <Grid item sm={12} lg={5} style={{marginTop:50, marginBottom:20}}>
                            <Typography variant="h4" style={{textAlign:"center",color:"#0eb0ee", marginTop:20 }}>
                                Digital Transformation <br/>
                                Making Chaos Cool!
                            </Typography>
                            <Typography variant="body" spacing={3} style={{textAlign:"left", marginTop:40, paddingLeft:5}}>
                                MEERA Data Foundation is a cloud-native petrotechnical master data repository focused on fully extensible data management solutions for oil and gas companies who need to manage seismic, well, geospatial and production datasets. 
                                A turnkey subsurface data repository solution, equipped with functionality that enables an accelerated digital transformation process with unparalleled insights and cost savings. The solution enables your organization to progressively build your data lake using all technical and operational data, 
                                enforcing data quality and data governance rules throughout your data lifecycle.
                                The solution provides the trusted-data foundation for a dynamic, structured data repository, 
                                built on modern IT and open standards, allowing customers to collaboratively manage all their data resources. It allows companies to self-develop data  solutions and operationalize advanced analytics and data science solutions to support, competitive business decisions at a fracture of the costs for conventional data integration services. 
                                The use of collaborative, online workflows for all aspects of content submission and distribution enables your community of stakeholders to interact with data through a simple user interface, which can be accessed from any mobile or desktop device.
                            </Typography>
                    </Grid>
                        
                    <Grid item sm={12} lg={6} style={{marginTop:50, marginBottom:20}}>
                        <img src="https://irp-cdn.multiscreensite.com/2c98796a/dms3rep/multi/desktop/Viewers2.png" width="100%" />           
                    </Grid>                   
                </Grid>
            </Paper>
            <Grid container direction="row" justify="center">
                <Grid item xs={12} sm={12} md={6} lg={8} xl={8} >
                    <Divider style={{ backgroundColor:"#0eb0ee" , height:3, marginTop:20, marginBottom:20}}/>
                </Grid>
            </Grid>
            <Grid container direction="row" justify="space-around" style={{backgroundColor:"#ecedef"}}>
                <Grid item xs={12} sm={12} md={3} lg={3} xl={3} >
                    <Typography variant="h6" className={classes.typographyHeading}>
                        Petrotechnical Data Management
                    </Typography>
                
                    <Typography variant="h6" className={classes.typographyContent}>
                        Deployed upon the agile MEERA Ecosystem, MEERA Data Foundation provides a complete end-to-end cloud solution for operators to 
                        implement a robust platform for data management and governance. The solution supports business activity across your entire 
                        master data lifecycle, including workflows for submission, verification and loading of data from data providers, search,
                        order and delivery to data consumers. With a single, integrated online user interface for all operations including public 
                        and registered user portals, administration and data loading.
                    </Typography>
                    
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3} xl={3} >
                    <Typography variant="h6" className={classes.typographyHeading}>
                        Advantages
                    </Typography>
                    
                    <Typography variant="h6" className={classes.typographyContent}>
                        Move your workloads to the  private, public or hybrid cloud and retire your legacy system.
                        Free your data from the shackles of proprietary apps and platforms
                        Create automated, intelligent data ingestion workflows to support your BI and ML requirements.
                        Generate a unified enterprise data ecosystem across all your operating units.
                        Build a System of Record for your unmanaged data types.
                        Create a common data foundation for application integration.
                        Produce a trusted data feed for your data lake.
                        Establish global reference data standards.
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3} xl={3} >
                    <Typography variant="h6" className={classes.typographyHeading}>
                        Benefits
                    </Typography>
                    
                    <Typography variant="h6" className={classes.typographyContent}>
                        Configurable Data Management Workspace.
                        Wells and Seismic Data Blueprint based on PPDM 3.9.
                        Scalable to handle both modest and large data deployments ranging from a few terabytes to several petabytes.
                        Visualizers for documents, well data and seismic data.
                        Built-in geospatial capability compliant with OGP standards.
                        OData API for integration with external analytics.
                        Data Virtualization Service with rich metadata and modelling.
                        Workflow Service (with audit trail).
                        Includes a web services API built using the GraphQLstandard.
                    </Typography>
                </Grid>
            </Grid>
            
            
        </div>
            
            
        
    )
}
export default EPSolution;
