import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Landing from './components/Layout/Landing/Landing';
import Navbar from './components/Layout/Navbar/Navbar'
import Footer from './components/Layout/Footer/Footer'
import MeeraEco from './components/Pages/MeeraEco';
import EPSolution from './components/Pages/EPSolutions';
import ProfessionalServices from './components/Pages/ProfessionalServices'
import News from './components/Pages/News';
import Contact from './components/Pages/Contact';

const useStyles = makeStyles(theme => ({
  root: {
    // minHeight: '100vh',
  },
 
}));

const App = () => {
  const classes = useStyles(); 
  return (
    <div className={classes.root}>
    <Router>
      <Navbar />
      <Switch>
        <Route exact path='/' component={Landing} />
        <Route exact path='/meera-ecosystem' component={MeeraEco} />
        <Route exact path='/ep-solution' component={EPSolution} />
        <Route exact path='/professional-services' component={ProfessionalServices} />
        <Route exact path='/news' component={News} />
        <Route exact path='/contact' component={Contact} />
      </Switch>
      <Footer />
    </Router>
    </div>
  );
}

export default App;
